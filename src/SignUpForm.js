import React from  'react';

function SignUpSuccess() {
    alert("Sign Up success, Now Login...");
  }

  class SignUpForm extends React.Component
{
    render()
    {
        return(
            <form>  
                <div class="form-group">
                    <h1>Welcome</h1>
                    <label for="uname">User Name:</label>
                    <input type="text" class="form-control" id="uname" placeholder="Enter user name..." name="username" required />
                </div>
                <div class="form-group">
                    <label for="uname">Phone Number:</label>
                    <input type="text" class="form-control" id="pno" placeholder="Enter phone number..." name="pno" required />
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password..." name="pswd" required />
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" name="remember" required /> I agree with the Terms and Conditions...
                    </label>
                </div>
                <button onClick={SignUpSuccess} type="submit" class="btn btn-primary" >Submit</button>              
            </form>
        )
    }
}


export default SignUpForm;