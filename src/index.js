import React from 'react';
import ReactDOM from 'react-dom';
import SignUpForm from './SignUpForm'

ReactDOM.render(
  <React.StrictMode>
    <SignUpForm />
  </React.StrictMode>,
  document.getElementById('root')
);
